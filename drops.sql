/*
	Author: Artur Finger
	
	POZOR
		- DROP TABLE automaticky dropuje svoje CONSTRAINTY, TRIGGERY a INDEXY
		- CASCADE CONSTRAINTS dropne i vsechny FK constrainty, kt. ukazujou na klice v tom to tablu
		- DORP PACKAGE dropuje automaticky i package body
*/


--generuje dropy
/*
SELECT 'DROP '|| OBJECT_TYPE ||' '|| OBJECT_NAME || DECODE(OBJECT_TYPE, 'TABLE', ' CASCADE CONSTRAINTS', '') || ';' as drop_prikaz
FROM USER_OBJECTS WHERE OBJECT_TYPE NOT IN('PACKAGE BODY','LOB', 'INDEX', 'TRIGGER')
ORDER BY OBJECT_TYPE, OBJECT_NAME DESC;
*/

PROMPT START: Rusi se vsechno ======================================<<<<<<<<<<<<;
                                                     
DROP PACKAGE PCK_ZISKEJ;                                                        
DROP PACKAGE PCK_STAT;                                                          
DROP PACKAGE PCK_PRIDEJ;                                                        
DROP PROCEDURE OVER_ZAVADNOST;                                                  
DROP SEQUENCE SEQ_ZAKONY;                                                       
DROP SEQUENCE SEQ_SVS_ZAVADY;                                                   
DROP SEQUENCE SEQ_PROVOZOVNY;                                                   
DROP SEQUENCE SEQ_OBORY;                                                        
DROP SEQUENCE SEQ_KONTROLY;                                                     
DROP SEQUENCE SEQ_COI_ZAKAZY;                                                   
DROP SEQUENCE SEQ_COI_ZAJISTENI;                                                
DROP SEQUENCE SEQ_COI_SANKCE;                                                   
DROP TABLE ZAKONY CASCADE CONSTRAINTS;                                          
DROP TABLE SVS_ZAVADY CASCADE CONSTRAINTS;                                      
DROP TABLE STAT_PROVOZOVNY CASCADE CONSTRAINTS;                                 
DROP TABLE STAT_POKUTY CASCADE CONSTRAINTS;                                     
DROP TABLE STAT_KONTROLY CASCADE CONSTRAINTS;                                   
DROP TABLE PROVOZOVNY CASCADE CONSTRAINTS;                                      
DROP TABLE PATRI CASCADE CONSTRAINTS;                                           
DROP TABLE OBORY CASCADE CONSTRAINTS;                                           
DROP TABLE KONTROLY CASCADE CONSTRAINTS;                                        
DROP TABLE DLE CASCADE CONSTRAINTS;                                             
DROP TABLE COI_ZAKAZY CASCADE CONSTRAINTS;                                      
DROP TABLE COI_ZAJISTENI CASCADE CONSTRAINTS;                                   
DROP TABLE COI_SANKCE CASCADE CONSTRAINTS;                                                                               
DROP VIEW V_VELKOOBCHODY;                                                       
DROP VIEW V_TOP3SANKCE;                                                         
DROP VIEW V_TOP10_SANKCE;                                                       
DROP VIEW V_TOP10_NEJZAVADNEJSICH;                                              
DROP VIEW V_TOP10_NEJSDRAZSICH_ZAKAZU;                                          
DROP VIEW V_TOP10_NEJLEPSICH_RESTAURACI;                                        
DROP VIEW V_TOP10_NEJKONTROLOVANEJSI;                                           
DROP VIEW V_SANKCE_PRUMER;                                                      
DROP VIEW V_SANKCE;                                                             
DROP VIEW V_POCET_ZAVAD;                                                        
DROP VIEW V_POCET_KONTROL;                                                      
DROP VIEW V_OBORY;                                                              
DROP VIEW V_KONTROLY_ZAVADNE;                                                   
DROP VIEW V_KONTROLY;                                                           
DROP VIEW V_INFO;                                                               
DROP VIEW V_DETAILY_KONTROL; 