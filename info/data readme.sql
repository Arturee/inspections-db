/*
	Author: Artur Finger
	
	Jsou 2 soubory se ukazkovymi daty:
		testrun - vlozeni malych demonstracnich dat pomoci balicku procedur (navic tam jsou dalsi ukazky funkcnosti)
		data big - realna data z praxe. (Nahravani trva neco pres hodinu. Podrobnosti viz "data big.sql".)
*/


-- nasledujici selekty lze pouzit na overeni, ze se opravdu nahralo vsechno z "data big"
SELECT DECODE(COUNT(*) , 16045, 'success', 'fail') AS provozovny FROM provozovny;
SELECT DECODE(COUNT(*) , 10477, 'success', 'fail') AS coi_sankce FROM coi_sankce;
SELECT DECODE(COUNT(*) , 17650, 'success', 'fail') AS coi_zajisteni FROM coi_zajisteni;
SELECT DECODE(COUNT(*) , 18028, 'success', 'fail') AS coi_zakazy FROM coi_zakazy;
SELECT DECODE(COUNT(*) , 92732, 'success', 'fail') AS dle FROM dle;
SELECT DECODE(COUNT(*) , 46238, 'success', 'fail') AS kontroly FROM kontroly;
SELECT DECODE(COUNT(*) , 794, 'success', 'fail') AS obory FROM obory;
SELECT DECODE(COUNT(*) , 14406, 'success', 'fail') AS patri FROM patri;
SELECT DECODE(COUNT(*) , 37, 'success', 'fail') AS zakony FROM zakony;