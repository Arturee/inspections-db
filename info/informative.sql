/*	
	Author: Artur Finger

	TT: 24h
	
	POZNAMKY
		- Vsechny dotazy z balicku a z pohledu jsou optimalizovane, aby pouzivaly maximalne 0, respektive 1 FULL SCAN.
		- Vsechno bezi rychle (pod 0.2s na velkych datech).
	
	  -----------------------------------------------------------
	  
	TODO odebirani provozoven/oboru
	TODO pridat nejake dalsi ukazky erroru do testrun.sql
	TODO viz cviko 12,13

	MEBBE vyzkouset statistiky spocitat z big dat
	MEBBE view: --vsechny velkoobchody v RADIUSU Prahy serazene od nejzavadnejsiho
	MEBBE ve statistikach casteji commitovat
	MEBBE zamknout pred statistikama?
	MEBBE silnejsi omezeni na souradnice
	MEBBE odstranit nejake >0 checky
	MEBBE dalsi triggery, api
	MEBBE jeden pck pro kazdou tabulku

	
	  -----------------------------------------------------------
	POZOR
		- SET DEFINE OFF povoluje aby ve srinzich byly specialni znaky jako (&, atd.)
		- precision * je 38
		- = NULL nefunguje. je potreba IS NULL.
		- oracle nema boolean. je potreba CHAR(1 BYTE) Y/N
		- AND ma preferenci pred OR
		? following queries are formatted client side and only then sent to the server (as other queries)
		? every unique column (or pk) is implemented with a special index => searching is fast (index search)
		- NUMBER (18,3) znamena fixed point [15].[3] (18 je jich dohromady) - ORACLE SQL
		- NUMERIC (18, 3) znamena 21 dohromady - ANSI SQL
		- vzdy pouzivat NVARCHAR  (ten pouzivat UTF (asi 8), na druhou stranu VARCHAR pouzivat 8-bit codepage (asi winodws-1250))
		- sequence maxvalue je defaultne 10^27
		- NUMBER je IEEE double, kt. se ruzne restrihuje podle parametru, ale dusledkem je ze maxvalue je cca 10^127 s presnosti na hornich 39 cislic
		- dual je pomocna tabulka (protoze slovicko FROM je v SELECTU mandatory)
		- procedury v tele procedur se volaji normalne C-ckovsky (mimo telo se museji volat pomoci EXEC! nebo obalene BEGIN+END;)
		- PL/SQL aby se blok kodu zkompiloval, je potreba / (na zacatku noveho radku)
			- tedy za kazdym package/function/procedure/standalone block musi byt /
			- za / nesmi nasledovat ani komentar!
		- nezle pouzivat double quotes ""
		- procedury/funkce v (ani v packagi) nemohou mit stejny nazev jako tabulky
		- DROP zahazuje veci do BINU, (pak maji jmeno BIN$...) odtud jdou zase vytahnout
		- "cannot preform a DML operation inside a query"! -- takze nelze pouzit funkci, ktera insertuje a pak vrati pouzite ID!
		- je zakazano pojmenovavat constrainty, pokud je sepecifikovano DEFAULT
			- ovsem pri poruseni constraintu klasicky vyvstane error a bohuzel ma tedy hnusene jmeno (takze se napriklad nedosadi automaticky default hodnota nebo neco podobneho)
		- nelze mit cosntraint datum <= SYSDATE
		- nelze udelat 1 trigger na vice tablu zaroven
		- ' se escapuje zdvojenim ('')
		- misto limit 3 se pouziva ROWNUM <= 3 (keyword limit nefunguje)
		- pojmenovane constrainty nemusi dostat sve jmeno (misto toho se jim vygeneruje systemove)
			- zda se ze to tak ma PRIMARY KEY, UNIQUE a NOT NULL
		- u INSERT + SELECT se stava, ze je rychlej plan samotnej pro SELECT, ale hodne pomalej plan pro INSERT + SELECT!
		- WITH klauzule nelze hnizdit
		- pri tvorbe view nelze mit nepojmenovane sloupecky (napr. COUNT(*) musi byt COUNT(*) AS pocet)
		- DECODE() umi provnat dve NULLy
		- DECODE() defaultne vraci NULL pro nezadane vstupy
		
		SQL DEVELOPER
		- kazdy package musi byt ve vlastnim souboru pri debugovani, jinak se nezobrazi vypis chyb
		- CTRL + SPACE = intellisense
		
	POZOR
		- primary key je not null (a samozrejme unique)
		- na primary key se automaticky vytvori index. (a na unique taky)
		- NVARCHAR2 je utf-8 (nebo 16)
		- Number(10,8) znamena 2 mista + 8 desetinnych mist
		- Number znamena IEEE double
		
		
--------------------------------




  HINTS
  
	pridavani nove kontroly
		-mozna by se hodilo pouzit coalesce(id, seq_id.nextval);
	porovnavani argumentu, ktere mohou byt null, ale pouzivaji se v selectu
		misto col = x,
		DECODE(x, col, 0) IS NOT NULL --vrati NULL pokud se nerovnaji a 0 jinak (i kdyz jsou oba NULL)
  
	zruseni diakritiky
		SELECT 'àéêöhello!' Collate SQL_Latin1_General_CP1253_CI_AI
	vytvoreni uplne novych idecek
		ALTER TABLE prov ADD prov_id NUBMER(*,0)
		UPDATE prov SET prov_id = seq_p.NEXTVAL;
  
  select table_name, column_name, nullable 
	from user_tab_cols where table_name = 'MYTABLE';

--------------------------------






*/
