Re�en� by melo obsahovat:

    + Definice potrebn�ch tabulek vcetne definic prim�rn�ch, kandid�tn�ch a ciz�ch kl�cu.
    + Definice vhodn�ch indexu vcetne tech pres v�echny ciz� kl�ce.
    + Definice dal��ch integritn�ch omezen� hl�daj�c�ch integritu zad�van�ch dat.
    + Triggeru hl�daj�c�ch veci, kter� na �rovni IO hl�dat nelze.
    + Neexistuj�c� u�ivatelsk� rozhran� by melo b�t simulov�no pohledy odpov�daj�c�mi jednotliv�m obrazovk�m. Napr.: 
       - Seznam z�kazn�ku vcetne �daju dota�en�ch z c�seln�ku 
       - Seznam v�pujcek vcetne �daju o z�kazn�c�ch a vypujcen�ch titulech.
       - Seznam v�pujcek po term�nu.
       - Seznam poctu voln�ch kusu od jednotlivych titulu
        atd. atd. (podle potreby aplikace)
    - Jednotliv� tabulky (rozhodne ty hlavn�) by mely m�t definov�ny bal�ky procedur, funkc� a dal��ch potrebn�ch objektu kter� manipuluj� s dan�mi entitami
obdobne jako v objektov�m programov�n� a reprezentuj� tak aplikacn� vrstvu. Procedury a funkce by mely hl�dat, zda jsou data smyslupln�.
Resp. mely by poc�tat s t�m, �e triggery a IO nekter� manipulace odm�tnou a korektne se k v�jimk�m zachovaj�. Pr�klad metod pro z�kazn�ky v pujcovne:
       - Prid�n� nov�ho z�kazn�ka.
       - Zru�en� z�kazn�ka (pokud nem� nic pujcen�ho)
       - Evidence nov� v�pujcky.
       - Vr�cen� konkr�tn� v�pujcky.
       - Vr�cen� v�ech v�pujcek z�kazn�ka.
       - Spocten� pen�le za preta�en� doby v�pujcky
       - atd. atd. (podle potreby aplikace)

- Sch�ma by melo b�t rozumne indexovan�, aby dotazy na v�echny data pohledu nevy�adovaly v�ce ne� jeden FULL-SCAN (na r�d�c� tabulku). 
SQL pr�kazy v procedur�ch a funkc�ch by krome oduvodnen�ch situac� nemely vy�adovat ��dn� FULL-SCAN, proto�e se manipuluje s omezenou sadou r�dek. 
- Dbejte na mo�nost paraleln�ho behu v�ce instanc� programu nad sd�len�mi daty. Kritick� sekce programu by mely vhodne vyu��vat transakcn�ch
schopnost� vcetne zamyk�n� tabulek a r�dek pro dosa�en� datov� konzistence.
- Souc�st� re�en� by mely b�t alespon n�sleduj�c� skripty:

    1 + Vytvoren� cel�ho sch�matu.
    2 + Vlo�en� demonstracn�ch dat
    3 + Vygenerov�n� statistik ke v�em tabulk�m sch�matu
    4 + Zru�en� statistik ke v�em tabulk�m sch�matu
    5 + Korektn� zru�en� cel�ho sch�matu 
    6 + Testovac� skript, kter� by uk�zal funkcnost v�ech navr�en�ch verejn�ch metod v procedur�ln�m rozhran� a funkcnost pohledu. Napr�klad:



    --jak vypadaji zakaznici pred zmenami?
    select * from pohled_na_zakazniky;
    -- pridame dalsiho zakaznika do seznamu zakazniku ...
    exec db_zakaznik.novy_zakaznik(...);
    -- ... nejakemu jinemu zakaznikovi zmenime adresu ...
    exec db_zakaznik.zmena_zakaznika(...);
    -- ... pokusime se smazat zakaznika s vypujckami ...
    exec db_zakaznik.zrus_zakaznika(...);
    -- ... a nejakeho zakaznika bez vypujcek
    exec db_zakaznik.zrus_zakaznika(...);
    -- jak vypadaji zakaznici po zmenach?
    select * from pohled_na_zakazniky;
    ...



- Skripty okomentujte 
    - U ka�d�ho skriptu by melo b�t mo�n� urcit autora. 
    - Ve skriptu 1) by melo b�t popsan� zad�n� �lohy. 
    - Skript 1) by mel popisovat v�znam jednotliv�ch objektu ve sch�matu, vcetne sloupcu tabulek a parametru procedur a funkc� 
    - Skript 6) by mel popisovat, co se dan�m vol�n�m testuje, a zvl�te to, �e vol�n� m� skoncit chybou (jakou, proc).
    - U slo�itej��ch SQL dotazu a pr�kazu uvedte, co delaj�. Bez koment�re se v�znam pr�kazu zpetne te�ko zji�tuje.
