/*
	Author: Artur Finger
	
	OBSAH:
		DEFINICE SCHEMATU
		SEKVENCE
		INDEXY
		TRIGGERY
		BALICKY PROCEDUR
		VIEWS - simulace klienta
		POMOCNE TABULKY A BALICEK PROCEDUR PRO POCITANI ROCNICH STATISTIKY
*/


PROMPT START: Vytvari se schema + triggery + package funkci  ======================================<<<<<<<<<<<<;

PROMPT definice schematu ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;
-----------LAYER 1
CREATE
/*
	Obor podnikani, do ktereho nejaka provozovna patri.
		napr. zpracovani drubeziho masa, chov dobytka, mlekarny, rybniky, obchod se zeleninou, atd.
*/
  TABLE obory
  (
    obory_id	NUMBER (*,0) CONSTRAINT obory_pk PRIMARY KEY CHECK (obory_id >= 0)
    ,nazev		NVARCHAR2 (150) CONSTRAINT obory_nnuq NOT NULL UNIQUE
  );
  

CREATE
/*
	Fyzicky nekde umistena nejak hospodarici provozovna, ve ktere lze provadet kontroly
		napr. farma, obchod, velkoobchod, stanek, rybnik, mrazirna, sklad, atd.
	Dontrolovat se muze napr. zdravi zvirat, pravdivost popisku zbozi, jestli zbozi neni prosle,
	dodrzeni hygienickych podminek, dodrzeni skladovani pri spravne teplote, atd.
*/
  TABLE provozovny
  (
    provozovny_id	NUMBER (*,0) CONSTRAINT provozovny_pk PRIMARY KEY CHECK (provozovny_id > 0)
    ,nazev			NVARCHAR2 (200)
    ,geo_lat		NUMBER (10,8) CONSTRAINT provozovny_ch_lat CHECK (geo_lat > 0.0) --zemepisna sirka (serverni)
    ,geo_lng		NUMBER (10,8) CONSTRAINT provozovny_ch_lng CHECK (geo_lng > 0.0) --zemepisna delka (vychodni)
    ,ico			CHAR (8 BYTE) CONSTRAINT provozovny_uq_ico UNIQUE CHECK (REGEXP_LIKE(ico, '^[0-9]{8}$'))
    ,adr_kraj		NVARCHAR2 (100)
    ,adr_obec		NVARCHAR2 (100)
    ,adr_okres		NVARCHAR2 (100)
    ,adr_ulice		NVARCHAR2 (100)
    ,adr_cp			NUMBER (*,0)
    ,adr_psc		CHAR (5 BYTE)
  );
  
CREATE
/*
	Zakon, jehoz dodrzeni kontroluje kontrolni organ (nejaka statni organizace)
		napr. §12, chlazena kurata museji byt pod teplotou 2°C; $432, nepresnost uvedene hmotnosti
		a opravdove hmotnosti produktu nesmi prekrocit 10g.; atd.
*/
  TABLE zakony
  (
    zakony_id	NUMBER (*,0) CONSTRAINT zakony_pk PRIMARY KEY CHECK (zakony_id > 0)
    ,paragraf	NVARCHAR2 (100)
    ,zneni		NVARCHAR2 (300)
	
	,CONSTRAINT zakony_uq UNIQUE(paragraf, zneni)
  );

-----------LAYER 2
CREATE
/*
	Pomocna tabulka, ktera urcuje kterym oborem podnikani se ktera provozovna zabyva.
	Kazda provozovna muze spadat do vice oboru a do kazdeho oboru vice provozoven.
*/
  TABLE patri
  (
    provozovny_id	NUMBER (*,0) CONSTRAINT patri_fk_pr REFERENCES provozovny(provozovny_id) NOT NULL
    ,obory_id		NUMBER (*,0) CONSTRAINT patri_fk_ob REFERENCES obory(obory_id) NOT NULL
	
    ,CONSTRAINT patri_pk PRIMARY KEY(provozovny_id, obory_id)
  );
  
CREATE
/*
	V provozovnach probihaji pravidelne kontroly nejakym statnim kontrolnim organem - bud COI nebo SVS.
	Ispektori namatkou vyberou ruzne objekty zajmu a ty zkontroluji, pokud nekde najdou zavadu,
	sepisi protokol. Inspektori COI mohou pozadovat penezitou sankci (pokud byl hrube porusen nejaky zakon),
	uvalit zakaz prodavani daneho vyrobku, nebo zajistit/sebrat rozumne mnozstvi daneho vyrobku (napr. pokud jde o padelek),
	nebo klidne vse najednou.
		viz tabulky COI_SANKCE, COI_ZAJISTENI, COI_ZAKAZY
	Inspektori SVS maji jednodussi praci a staci, kdyz sepisi co bylo za problem.
		viz SVS_ZAVADY
	V obou pripadech je ale zaznamenavano datum a zda byla kontrola zavadna nebo ne.
*/
  TABLE kontroly
  (
    kontroly_id		NUMBER (*,0) CONSTRAINT kontroly_pk PRIMARY KEY CHECK (kontroly_id > 0)
    ,je_zavadna		CHAR (1 BYTE) DEFAULT 'N' NOT NULL CHECK (je_zavadna IN ('Y', 'N')) --yes, no
    ,datum			DATE DEFAULT SYSDATE NOT NULL
    ,provozovny_id	NUMBER (*,0) CONSTRAINT kontroly_fk REFERENCES provozovny(provozovny_id)
  );

-----------LAYER 3
CREATE
/*
	Pomocna tabulka, ktera urcuje zakony, jejichz dodrzenim se zabyvala dana kontrola.
	Kazda kontrola se muze zabyvat vice zakony a dodrzeni kazdeho zakona muze byt overovano vice ruznymi kontrolami.
*/
  TABLE dle
  (
    kontroly_id		NUMBER (*,0) CONSTRAINT dle_fk_ko REFERENCES kontroly(kontroly_id) NOT NULL
    ,zakony_id		NUMBER (*,0) CONSTRAINT dle_fk_za REFERENCES zakony(zakony_id) NOT NULL
	
    ,CONSTRAINT dle_pk PRIMARY KEY(kontroly_id, zakony_id)
  );
  
CREATE
/*
	Inspektori COI mohou udelit penezitou sankci pri nejake kontrole a meli by uvest do
	kdy musi sankce byt splacena (datum nabyti pravni moci).
		napr. 20.5.2016, 500 000, atd.
*/
  TABLE coi_sankce
  (
    coi_sankce_id	NUMBER (*,0) CONSTRAINT coi_sankce_pk PRIMARY KEY CHECK (coi_sankce_id > 0)
    ,vyse_pokuty	NUMBER (*,0) CONSTRAINT coi_sankce_ch CHECK (vyse_pokuty >= 0) --v korunach
    ,datum_npm		DATE	--datum nabyti pravni moci
    ,kontroly_id	NUMBER (*,0) CONSTRAINT coi_sankce_fk REFERENCES kontroly(kontroly_id)
  );
  
CREATE
/*
	Inspektori COI mohou zajistit/sebrat nejake vyrobky (treba padelky). Musi pak uvest
	o co jde, znacku vyrobku, mnozstvi a ktery zakon jim dovolil zajisteni provest.
		napr. Adidas, obuv, 40, Zakon proti padelkum §32, atd.
*/
  TABLE coi_zajisteni
  (
    coi_zajisteni_id	NUMBER (*,0) CONSTRAINT coi_zajisteni_pk PRIMARY KEY CHECK (coi_zajisteni_id > 0)
    ,znacka				NVARCHAR2 (100)
    ,vyrobek			NVARCHAR2 (200)
    ,mnozstvi			NUMBER CONSTRAINT coi_zajisteni_ch CHECK (mnozstvi > 0.0)
    ,dle_zakona			NVARCHAR2 (300)
    ,kontroly_id		NUMBER (*,0) CONSTRAINT coi_zajisteni_fk REFERENCES kontroly(kontroly_id)
  );
  
CREATE
/*
	Inspektori COI mohou zakazat prodej nejakeho zbozi, napr. pokud je zbozi zivotu nebezpecne
	nebo nevhodne pro deti. Museji pak uvest o co jde, mnozstvi, jednotku, celkovou cenu toho co
	bylo zakazane prodavat a zakon, podle ktereho zakaz uvalili.
		napr. detska hracka, 12, kus, 1230, Zakon proti pouzivani skodlivych chemikalii v detskych hrackach §11, atd.
*/
  TABLE coi_zakazy
  (
    coi_zakazy_id	NUMBER (*,0) CONSTRAINT coi_zakazy_pk PRIMARY KEY CHECK (coi_zakazy_id > 0)
    ,sortiment		NVARCHAR2 (100)
    ,mnozstvi		NUMBER CONSTRAINT coi_zakazy_ch_mn CHECK (mnozstvi > 0.0)
    ,jednotka		NVARCHAR2 (20)
    ,cena_celkem	NUMBER (*,0) CONSTRAINT coi_zakazy_ch_ce CHECK (cena_celkem > 0)
    ,dle_zakona		NVARCHAR2 (300)
    ,kontroly_id	NUMBER (*,0) CONSTRAINT coi_zakazy_fk REFERENCES kontroly(kontroly_id)
  );
  
CREATE
/*
	Inspektri SVS maji povinnost sepsat do protokolu zavady, ktere zjistili a dat
	tomu nejaky titulek.
		napr. Nizka teplota, Teplota mrazici jednotky na zmrzlinu prevysila zakonem pozadovanou
		minimalni hranici 4°C o celych 15°C.
*/
  TABLE svs_zavady
  (
    svs_zavady_id	NUMBER (*,0) CONSTRAINT svs_zavady_pk PRIMARY KEY CHECK (svs_zavady_id > 0)
    ,titulek		NVARCHAR2 (100)
    ,popis			NVARCHAR2 (300)
    ,kontroly_id	NUMBER (*,0) CONSTRAINT svs_zavady_fk REFERENCES kontroly(kontroly_id)
  );
    
/*
	Vetsina atributu vyse muze byt null, jelikoz nekdy je nutne chranit udaje fyzickych osob - tim, ze se do databaze nezapise vsechno.
*/


PROMPT sekvence + indexy ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;

CREATE SEQUENCE seq_provozovny;
CREATE SEQUENCE seq_kontroly;
CREATE SEQUENCE seq_obory;
CREATE SEQUENCE seq_zakony;
CREATE SEQUENCE seq_coi_sankce;
CREATE SEQUENCE seq_coi_zajisteni;
CREATE SEQUENCE seq_coi_zakazy;
CREATE SEQUENCE seq_svs_zavady;

CREATE INDEX patri_idx_p ON patri(provozovny_id);
CREATE INDEX patri_idx_o ON patri(obory_id);
CREATE INDEX kontroly_idx ON kontroly(provozovny_id);
CREATE INDEX dle_idx_k ON dle(kontroly_id);
CREATE INDEX dle_idx_z ON dle(zakony_id);
CREATE INDEX coi_sankce_idx ON coi_sankce(kontroly_id);
CREATE INDEX coi_zajisteni_idx ON coi_zajisteni(kontroly_id);
CREATE INDEX coi_zakazy_idx ON coi_zakazy(kontroly_id);
CREATE INDEX svs_zavady_idx ON svs_zavady(kontroly_id);

CREATE BITMAP INDEX kontroly_bidx ON kontroly(je_zavadna);


  

PROMPT triggery ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;

--vsechny triggery zajistuji, ze pokud je kontrola zavadna, tak k ni musi existovat nejaka sankce,zavada,zakaz nebo zajisteni
CREATE OR REPLACE PROCEDURE over_zavadnost(
id	kontroly.kontroly_id%TYPE
)AS
n	NUMBER;
BEGIN
SELECT COUNT(*) INTO n FROM kontroly WHERE kontroly_id = id AND je_zavadna = 'Y';
IF n <> 1 THEN
	RAISE_APPLICATION_ERROR(-20002, 'Error: nelze pridavat detaily zavady k nezavadne kontrole');
END IF;
END;
/

CREATE OR REPLACE TRIGGER coi_zakazy_trig AFTER INSERT ON coi_zakazy
FOR EACH ROW
BEGIN
over_zavadnost(:NEW.kontroly_id);
END;
/
CREATE OR REPLACE TRIGGER coi_zajisteni_trig AFTER INSERT ON coi_zajisteni
FOR EACH ROW
BEGIN
over_zavadnost(:NEW.kontroly_id);
END;
/
CREATE OR REPLACE TRIGGER coi_sankce_trig AFTER INSERT ON coi_sankce
FOR EACH ROW
BEGIN
over_zavadnost(:NEW.kontroly_id);
END;
/
CREATE OR REPLACE TRIGGER svs_zavady_trig AFTER INSERT ON svs_zavady
FOR EACH ROW
BEGIN
over_zavadnost(:NEW.kontroly_id);
END;
/


PROMPT baliky funkci - hlavicky ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;

CREATE OR REPLACE PACKAGE pck_ziskej ---------------------------------------------------------------------
AS

FUNCTION id_za_ico(
ic		provozovny.ico%TYPE
)
RETURN	provozovny.provozovny_id%TYPE;

END pck_ziskej;
/

CREATE OR REPLACE PACKAGE pck_pridej ---------------------------------------------------------------------
AS
---------
  
PROCEDURE provozovnu (
/*
	Zaregistruje novou provozovnu, na ktere nyni bude mozne provadet kontroly.
	Vhodne kdykoliv vznikne nejaka firma/hospodarstvi a zaregistruje se do ARES.
	
	Id je potreba preden ziskat ze sequence seq_provozovny
*/
id		provozovny.provozovny_id%TYPE
,nazev	provozovny.nazev%TYPE
,lat	provozovny.geo_lat%TYPE  --zemepisna sirka (serverni)
,lng	provozovny.geo_lng%TYPE  --zemepisna delka (vychodni)
,ico	provozovny.ico%TYPE
,kraj	provozovny.adr_kraj%TYPE
,obec	provozovny.adr_obec%TYPE
,okres	provozovny.adr_okres%TYPE
,ulice	provozovny.adr_ulice%TYPE
,cp		provozovny.adr_cp%TYPE
,psc	provozovny.adr_psc%TYPE
);

PROCEDURE obor (
/*
	Specifikuje, do ktereho oboru spada dana provozovna. Napr. melkarna, slepicarna, mrazirna, velkoobchod, atd.
	Obor v databazi jeste neni, a musi se nejdrive pridat. Ovsem procedura pro jistotu nejdrive overi, zda
	obor v databazi opravdu neni a pokud je, nebude vytvaret novy zaznam, ale pouzije nalezeny.
*/
id_provozovny	provozovny.provozovny_id%TYPE
,nazev_oboru	obory.nazev%TYPE
);

PROCEDURE obor (
/*
	To same, akorat obor v databazi jiz je, a mame jeho id. (Uzivatel zakliknuj ve webove aplikaci nejakou polozku ze seznamu).
		SELECT * FROM oboy;
*/
id_provozovny	provozovny.provozovny_id%TYPE
,id_oboru		obory.obory_id%TYPE
); 

PROCEDURE kontrolu (
/*
	Prida zaznam o provedene kontrole nejake jiz zaregistrovane provozovny.
	
	Id je potreba predem ziskat ze seqence seq_kontroly
*/
id				kontroly.kontroly_id%TYPE
,id_provozovny	kontroly.provozovny_id%TYPE --lze ziskat pouzitim pck_ziskej.ico_za_id
,je_zavadna		kontroly.je_zavadna%TYPE DEFAULT 'N'
,datum			NVARCHAR2 DEFAULT TO_CHAR(SYSDATE, 'DD.MM.YYYY')
);

PROCEDURE zakon (
/*
	Specifikuje, podle ktereho zakona se dana kontrola provadi.
	Zakon v databazi jeste neni a musi se nejdrive pridat. Ovsem procedura pro jistotu nejdrive overi, zda
	zakon v databazi opravdu neni a pokud je, nebude vytvaret novy zakon, ale pouzije nalezeny.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,paragraf	zakony.paragraf%TYPE
,zneni		zakony.zneni%TYPE
);  
PROCEDURE zakon (
/*
	To same, akorat zakon v databazi uz je a mame jeho id. (Uzivatel zakliknuj ve webove aplikaci nejakou polozku ze seznamu).
		SELECT * FROM zakony;
*/
id_kontroly	kontroly.kontroly_id%TYPE
,id_zakona	zakony.zakony_id%TYPE
);

PROCEDURE coi_san (
/*
	Prida zaznam o sankci uvrzene pri nejake COI kontrole.
	npm = nabyti pravni moci (DD.MM.YYYY)
*/
id_kontroly		kontroly.kontroly_id%TYPE
,vyse_pokuty	coi_sankce.vyse_pokuty%TYPE
,datum_npm		NVARCHAR2
);

PROCEDURE coi_zaj (
/*
	Prida zaznam o zajisteni (nejakeho majetku), ke kteremu doslo v prubehu nejake COI kontroly.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,znacka		coi_zajisteni.znacka%TYPE
,vyrobek	coi_zajisteni.vyrobek%TYPE
,mnozstvi	coi_zajisteni.mnozstvi%TYPE
,dle_zakona	coi_zajisteni.dle_zakona%TYPE
);
  
PROCEDURE coi_zak (
/*
	Prida zaznam o zakazu (prodeje), ke kteremu doslo v prubehu nejake COI kontroly.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,sortiment		coi_zakazy.sortiment%TYPE
,mnozstvi		coi_zakazy.mnozstvi%TYPE
,jednotka		coi_zakazy.jednotka%TYPE
,cena_celkem	coi_zakazy.cena_celkem%TYPE
,dle_zakona		coi_zakazy.dle_zakona%TYPE
);

PROCEDURE svs_zav (
/*
	Prida zaznam o nalezenych problemech pri nejake SVS kontrole.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,titulek	svs_zavady.titulek%TYPE
,popis		svs_zavady.popis%TYPE
);

END pck_pridej;
/



PROMPT baliky funkci - tela ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;

CREATE OR REPLACE PACKAGE BODY pck_ziskej ---------------------------------------------------------------------
AS

FUNCTION id_za_ico(
ic		provozovny.ico%TYPE
)
RETURN	provozovny.provozovny_id%TYPE
AS
id 		provozovny.provozovny_id%TYPE;
BEGIN
SELECT provozovny_id INTO id FROM provozovny WHERE ic = ico;
RETURN id;
EXCEPTION
WHEN NO_DATA_FOUND THEN
RAISE_APPLICATION_ERROR(-20002, 'Error: ico ('|| ic ||') neni v databazi');
RETURN NULL;
END id_za_ico;

END pck_ziskej;
/


CREATE OR REPLACE PACKAGE BODY pck_pridej ---------------------------------------------------------------------
AS
-----------
    
PROCEDURE provozovnu (
id		provozovny.provozovny_id%TYPE
,nazev	provozovny.nazev%TYPE
,lat	provozovny.geo_lat%TYPE
,lng	provozovny.geo_lng%TYPE
,ico	provozovny.ico%TYPE
,kraj	provozovny.adr_kraj%TYPE
,obec	provozovny.adr_obec%TYPE
,okres	provozovny.adr_okres%TYPE
,ulice	provozovny.adr_ulice%TYPE
,cp		provozovny.adr_cp%TYPE
,psc	provozovny.adr_psc%TYPE
)AS
BEGIN
INSERT INTO provozovny (provozovny_id, nazev, geo_lat ,geo_lng, ico, adr_kraj, adr_obec, adr_okres, adr_ulice, adr_cp, adr_psc)
VALUES (id, nazev, lat, lng, ico, kraj, obec, okres, ulice, cp, psc);
END provozovnu;

PROCEDURE obor (
id_provozovny	provozovny.provozovny_id%TYPE
,id_oboru		obory.obory_id%TYPE
)AS
BEGIN
INSERT INTO patri(obory_id, provozovny_id) VALUES (id_oboru, id_provozovny);
END obor;

PROCEDURE obor (
id_provozovny	provozovny.provozovny_id%TYPE
,nazev_oboru	obory.nazev%TYPE
)AS
ido				obory.obory_id%TYPE;
BEGIN
SELECT obory_id INTO ido FROM obory WHERE nazev = nazev_oboru; --TODO lock? muze vyhodit uniqe constraint breach
pck_pridej.obor(id_provozovny, ido);
EXCEPTION
WHEN NO_DATA_FOUND THEN
	BEGIN
	SELECT seq_obory.NEXTVAL INTO ido FROM dual;
	INSERT INTO obory(obory_id, nazev) VALUES (ido, nazev_oboru);
	pck_pridej.obor(id_provozovny, ido);
	END;
END obor;

--datum ve formatu DD.MM.YYYY
PROCEDURE kontrolu (
id				kontroly.kontroly_id%TYPE
,id_provozovny	kontroly.provozovny_id%TYPE --lze ziskat pouzitim pck_ziskej.ico_za_id
,je_zavadna		kontroly.je_zavadna%TYPE DEFAULT 'N'
,datum			NVARCHAR2 DEFAULT TO_CHAR(SYSDATE, 'DD.MM.YYYY')
)AS
dat				DATE;
BEGIN
--pokud id, zavadna nebo datum je null tak automaticky error
--pokud provozovny_id neni FK tak automaticky error
dat := TO_DATE(TRIM(datum), 'DD.MM.YYYY');
IF dat > SYSDATE THEN
		RAISE_APPLICATION_ERROR(-20002, 'Error: nelze zadat datum v budoucnosti. (je moc vysoke)');
END IF;
INSERT INTO kontroly(kontroly_id, je_zavadna, datum, provozovny_id) VALUES (id, je_zavadna, dat, id_provozovny);
EXCEPTION
WHEN OTHERS THEN
	IF dat IS NULL THEN
		RAISE_APPLICATION_ERROR(-20002, 'Error: datum neni zadane ve formatu DD.MM.YYYY (spravny priklad: 30.01.2015)');
	ELSE
		RAISE;
	END IF;
END kontrolu;
 

PROCEDURE zakon (
/*
	Specifikuje, podle ktereho zakona se dana kontrola provadi.
	Zakon v databazi jeste neni a musi se nejdrive pridat. Ovsem procedura pro jistotu nejdrive overi, zda
	zakon v databazi opravdu neni a pokud je, nebude vytvaret novy zakon, ale pouzije nalezeny.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,paragraf	zakony.paragraf%TYPE
,zneni		zakony.zneni%TYPE
)AS
idz zakony.zakony_id%TYPE;
BEGIN
SELECT zakony_id INTO idz FROM zakony WHERE DECODE (paragraf, zakony.paragraf, 0) IS NOT NULL  --TODO lock? muze vyhodit uniqe constraint breach
	AND DECODE(zneni, zakony.zneni, 0) IS NOT NULL;
zakon(id_kontroly, idz);
EXCEPTION
WHEN NO_DATA_FOUND THEN
	BEGIN
	SELECT seq_zakony.NEXTVAL INTO idz FROM dual;
	INSERT INTO zakony(zakony_id, paragraf, zneni) VALUES (idz, paragraf, zneni);
	zakon(id_kontroly, idz);
	END;
END zakon;
PROCEDURE zakon (
/*
	To same, akorat zakon v databazi uz je a mame jeho id. (Uzivatel zakliknuj ve webove aplikaci nejakou polozku ze seznamu).
*/
id_kontroly	kontroly.kontroly_id%TYPE
,id_zakona	zakony.zakony_id%TYPE
)AS
BEGIN
INSERT INTO dle(kontroly_id, zakony_id) VALUES (id_kontroly, id_zakona);
END zakon;

PROCEDURE coi_san (
id_kontroly		kontroly.kontroly_id%TYPE
,vyse_pokuty	coi_sankce.vyse_pokuty%TYPE
,datum_npm		NVARCHAR2
)
AS
id				coi_sankce.coi_sankce_id%TYPE;
BEGIN
SELECT seq_coi_sankce.NEXTVAL INTO id FROM dual;
INSERT INTO coi_sankce(coi_sankce_id, vyse_pokuty, datum_npm, kontroly_id) VALUES (id, vyse_pokuty, TO_DATE(datum_npm, 'DD.MM.YYYY'), id_kontroly);
END coi_san;

PROCEDURE coi_zaj (
/*
	Prida zaznam o zajisteni (nejakeho majetku), ke kteremu doslo v prubehu nejake COI kontroly.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,znacka		coi_zajisteni.znacka%TYPE
,vyrobek	coi_zajisteni.vyrobek%TYPE
,mnozstvi	coi_zajisteni.mnozstvi%TYPE
,dle_zakona	coi_zajisteni.dle_zakona%TYPE
)AS
id			coi_zajisteni.coi_zajisteni_id%TYPE;
BEGIN
SELECT seq_coi_zajisteni.NEXTVAL INTO id FROM dual;
INSERT INTO coi_zajisteni(coi_zajisteni_id, znacka, vyrobek, mnozstvi, dle_zakona, kontroly_id)
	VALUES (id, znacka, vyrobek, mnozstvi, dle_zakona, id_kontroly);
END coi_zaj;

PROCEDURE coi_zak (
/*
	Prida zaznam o zakazu (prodeje), ke kteremu doslo v prubehu nejake COI kontroly.
*/
id_kontroly		kontroly.kontroly_id%TYPE
,sortiment		coi_zakazy.sortiment%TYPE
,mnozstvi		coi_zakazy.mnozstvi%TYPE
,jednotka		coi_zakazy.jednotka%TYPE
,cena_celkem	coi_zakazy.cena_celkem%TYPE
,dle_zakona		coi_zakazy.dle_zakona%TYPE
)AS
id				coi_zakazy.coi_zakazy_id%TYPE;
BEGIN
SELECT seq_coi_zakazy.NEXTVAL INTO id FROM dual;
INSERT INTO coi_zakazy(coi_zakazy_id, sortiment, mnozstvi, jednotka, cena_celkem, dle_zakona, kontroly_id)
	VALUES (id,  sortiment, mnozstvi, jednotka, cena_celkem, dle_zakona, id_kontroly);
END coi_zak;

PROCEDURE svs_zav (
/*
	Prida zaznam o nalezenych problemech pri nejake SVS kontrole.
*/
id_kontroly	kontroly.kontroly_id%TYPE
,titulek	svs_zavady.titulek%TYPE
,popis		svs_zavady.popis%TYPE
)AS
id			svs_zavady.svs_zavady_id%TYPE;
BEGIN
SELECT seq_svs_zavady.NEXTVAL INTO id FROM dual;
INSERT INTO svs_zavady(svs_zavady_id, titulek, popis, kontroly_id)
	VALUES (id, titulek, popis, id_kontroly);
END svs_zav;

-------------
END pck_pridej;
/



PROMPT pohledy (simulace klienta databaze) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;
/*
	Tyto pohledy simuluji pouziti databaze napr. nejakou webovou strankou.
		- pro hezkou demostraci je potreba nahrat data z 'data big.sql', misto z 'data small.sql'
		- pohledy nejsou urcene na vkladani dat, ale pouze pro cteni
*/

--desemantizovane detaily kontrol
CREATE VIEW v_detaily_kontrol AS
	SELECT CAST('Sankce - pokuta: ' || vyse_pokuty || ', datum nabyti pravni moci: ' || datum_npm AS NVARCHAR2(300))
		AS detail, kontroly_id FROM coi_sankce
	UNION
	SELECT 'Zajisteni - znacka: ' || znacka || ', vyrobek: ' || vyrobek || ', mnozstvi: ' || mnozstvi || ', dle zakona: ' ||
		dle_zakona AS detail, kontroly_id FROM coi_zajisteni
	UNION
	SELECT 'Zakaz - sortiment: ' || sortiment || ', mnozstvi: ' || mnozstvi || ', jednotka: ' || jednotka ||
		', cena celkem: ' || CENA_CELKEM || ', dle zakona: ' || dle_zakona AS detail, kontroly_id FROM coi_zakazy
	UNION
	SELECT 'Zavada - titulek: ' || titulek || ', popis: ' || popis AS detail, kontroly_id FROM svs_zavady;
	
	
--informace o jednom icu
CREATE VIEW v_info AS
	SELECT nazev, ico, geo_lat, geo_lng, adr_kraj,adr_obec,adr_okres,adr_ulice,adr_cp, adr_psc FROM provozovny WHERE ico = '60193328';
CREATE VIEW v_obory AS
	SELECT nazev AS obor FROM obory JOIN patri USING(obory_id) WHERE provozovny_id = pck_ziskej.id_za_ico('60193328');
--pocet zavadnych kontrol
CREATE VIEW v_pocet_zavad AS
	SELECT COUNT(*) AS zavady FROM kontroly WHERE provozovny_id = pck_ziskej.id_za_ico('60193328') AND je_zavadna = 'Y';
--pocet kontrol
CREATE VIEW v_pocet_kontrol AS
	SELECT COUNT(*) AS kontroly FROM kontroly WHERE provozovny_id = pck_ziskej.id_za_ico('60193328');
--sankce
CREATE VIEW v_sankce AS
	SELECT vyse_pokuty FROM kontroly JOIN coi_sankce USING(kontroly_id) WHERE provozovny_id = pck_ziskej.id_za_ico('60193328');
--prumerne rocni sankce
CREATE VIEW v_sankce_prumer AS
	WITH d AS (
		SELECT * FROM kontroly WHERE provozovny_id = pck_ziskej.id_za_ico('60193328'))
	SELECT s/yrs AS prumer_sankci FROM
		(SELECT GREATEST(FLOOR(MONTHS_BETWEEN(MAX(datum), MIN(datum)) /12), 1) AS yrs FROM d)
	CROSS JOIN
		(SELECT SUM(vyse_pokuty) AS s FROM d JOIN coi_sankce USING(kontroly_id));
--3 nejvyssi sankce a vsechny detaily k te kontrole
CREATE VIEW v_top3sankce AS
	SELECT datum, vyse_pokuty, datum_npm, paragraf, zneni FROM
		(SELECT datum, vyse_pokuty, datum_npm, paragraf, zneni FROM kontroly JOIN coi_sankce USING(kontroly_id)
		LEFT JOIN (dle JOIN zakony USING(zakony_id)) USING (kontroly_id) WHERE provozovny_id = pck_ziskej.id_za_ico('60193328')
		ORDER BY vyse_pokuty DESC)
	WHERE ROWNUM <= 3;
	
	
--detaily vsech kontrol
CREATE VIEW v_kontroly AS
	SELECT datum, je_zavadna, paragraf AS dle_zakona_paragraf, zneni AS dle_zakona_zneni, detail
	FROM kontroly LEFT JOIN (dle JOIN zakony USING(zakony_id)) USING(kontroly_id) LEFT JOIN v_detaily_kontrol USING(kontroly_id);
--detaliy vsech zavadnych kontrol (vcetne zakonu)
CREATE VIEW v_kontroly_zavadne AS
	SELECT * FROM v_kontroly WHERE JE_ZAVADNA = 'Y';
--10 nejvyssich sankci v republice
CREATE VIEW v_top10_sankce AS
SELECT datum, ico, vyse_pokuty FROM
	(SELECT * FROM
		(SELECT kontroly_id, vyse_pokuty FROM coi_sankce ORDER BY vyse_pokuty DESC)
	WHERE ROWNUM <= 10) LEFT JOIN kontroly USING(kontroly_id) LEFT JOIN provozovny
	USING(provozovny_id) ORDER BY vyse_pokuty DESC;
--10 nejkontrolovanejsich provozoven v republice
CREATE VIEW v_top10_nejkontrolovanejsi AS
	SELECT ico, pocet_kontrol FROM
		(SELECT ico, COUNT(*) AS pocet_kontrol FROM kontroly JOIN provozovny USING(provozovny_id) GROUP BY ico ORDER BY pocet_kontrol DESC)
	WHERE ROWNUM <= 10;
--10 nejzavadnejsich provozoven v republice
/*
--tato verze pouzivala dva FULL SCANY, pricemz jeden byl zbytecny

CREATE VIEW v_top10_nejzavadnejsich AS
	SELECT ico, pocet_kontrol FROM
		(SELECT ico, COUNT(*) AS pocet_kontrol FROM
			(SELECT provozovny_id FROM kontroly WHERE je_zavadna = 'Y')
			JOIN provozovny USING(provozovny_id) GROUP BY ico ORDER BY pocet_kontrol DESC)
	WHERE ROWNUM <= 10;
	
*/
CREATE VIEW v_top10_nejzavadnejsich AS
	SELECT ico, pocet_kontrol FROM
		(SELECT /*+INDEX(provozovny provozovny_pk)*/ ico, pocet_kontrol FROM 
			(SELECT provozovny_id, COUNT(*) AS pocet_kontrol FROM kontroly WHERE je_zavadna = 'Y' GROUP BY provozovny_id)
			JOIN provozovny USING(provozovny_id) ORDER BY pocet_kontrol DESC)
	WHERE ROWNUM <= 10;
--10 nejdrazsich zakazu a co bylo zakazano
CREATE VIEW v_top10_nejsdrazsich_zakazu AS
	SELECT datum, ico, cena FROM
		(SELECT * FROM
			(SELECT kontroly_id, cena_celkem AS cena FROM coi_zakazy ORDER BY cena DESC)
		WHERE ROWNUM <= 10) LEFT JOIN kontroly USING(kontroly_id) LEFT JOIN provozovny
		USING(provozovny_id) ORDER BY cena DESC;
--10 procentuelne nejmene zavadnych restauraci
CREATE VIEW v_top10_nejlepsich_restauraci AS
	WITH kontroly_restauraci AS (
		SELECT ico, kontroly_id, je_zavadna FROM
		provozovny JOIN (SELECT provozovny_id, nazev AS obor FROM patri JOIN obory USING(obory_id) WHERE nazev = '56100')
		USING(provozovny_id) JOIN kontroly USING(provozovny_id)
	) --CZ-NACE 56100 = Stravování v restauracích, u stánků a v mobilních zařízeních
	SELECT ico, perc FROM
		(SELECT ico, ROUND(COALESCE(zav,0)/vse,2) AS perc FROM
			(SELECT ico, COUNT(*) AS vse FROM kontroly_restauraci GROUP BY ico)
			LEFT JOIN
			(SELECT ico, COUNT(*) AS zav FROM kontroly_restauraci WHERE je_zavadna = 'Y' GROUP BY ico)
			USING(ico) WHERE vse > 0 ORDER BY perc ASC)
		WHERE ROWNUM <= 10;
--ica a nazvy a souradnice vsech velkoobchodu
CREATE VIEW v_velkoobchody AS --CZ-NACE 46* = vsechny mozne velkoobchody
	SELECT ico, nazev, geo_lat, geo_lng FROM provozovny JOIN --(data z "data big.sql" bohuzel neobsahuji souradnice)
		(SELECT provozovny_id FROM patri JOIN obory USING(obory_id) WHERE nazev LIKE '46%')
    USING (provozovny_id);
	

--jednotlive views lze spustit pomoci:
/* 
SELECT * FROM V_DETAILY_KONTROL; 

SELECT * FROM V_INFO; 
SELECT * FROM V_OBORY;
SELECT * FROM V_POCET_ZAVAD; 
SELECT * FROM V_POCET_KONTROL;
SELECT * FROM V_SANKCE;
SELECT * FROM V_SANKCE_PRUMER;
SELECT * FROM V_TOP3SANKCE;  

SELECT * FROM V_KONTROLY;
SELECT * FROM V_KONTROLY_ZAVADNE; 
SELECT * FROM V_TOP10_SANKCE; 
SELECT * FROM V_TOP10_NEJKONTROLOVANEJSI;
SELECT * FROM V_TOP10_NEJZAVADNEJSICH;
SELECT * FROM V_TOP10_NEJSDRAZSICH_ZAKAZU;  
SELECT * FROM V_TOP10_NEJLEPSICH_RESTAURACI;
SELECT * FROM V_VELKOOBCHODY;  
*/


--vygenerovani vseho vyse
--SELECT 'SELECT * FROM ' || view_name || ';' FROM USER_VIEWS;   


PROMPT pomocne schema + balicke pro rocni statistiky ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;
/*
	Jednou za rok se muze spustit procedura, ktera zaznamena mensi prehled o datech v databazi.
	Zaznamy z tohoto prehledu se budou ukladat do jednoduchych pomocnych tabulek stat_*.
	Statistiky se pocitaji umulovane (tedy vzdy z cele databaze, nikoliv jen z dat za uplynuly rok).

	OBAH
		pocet provozoven (per all/kraj/obec/okres)
		pocet kontrol (zavadnych/nezavadnych, procentuelne - per all/kraj/obec/okres)
		coi pokuty	(celkem, nejmensi, nejvetsi, prumerna - per all/kraj/obec/okres)
*/

CREATE
  TABLE stat_provozovny
  (
	k_datu			DATE
	,lokace			NVARCHAR2(100)
	,cleneni		NVARCHAR2(50)
	,pocet			NUMBER(*,0)
  );
  
CREATE
  TABLE stat_kontroly
  (
	k_datu				DATE
	,od					DATE
	,lokace				NVARCHAR2(100)
	,cleneni			NVARCHAR2(50)
	,pocet_zavadne		NUMBER(*,0)
	,proc_zavadne		NUMBER(5,4)
	,pocet_nezavadne	NUMBER(*,0)
	,proc_nezavadne		NUMBER(5,4)
	,pocet				NUMBER(*,0)
  );
  
CREATE
  TABLE stat_pokuty
  (
	k_datu				DATE
	,od					DATE
	,lokace				NVARCHAR2(100)
	,cleneni			NVARCHAR2(50)
	,minimalni			NUMBER(*,0)
	,maximalni			NUMBER(*,0)
	,prumerna			NUMBER(*,0)
	,suma				NUMBER(*,0)
	,pocet_pokut		NUMBER(*,0)
  );
  

CREATE OR REPLACE PACKAGE pck_stat
AS

PROCEDURE statistiky_provozoven;
PROCEDURE statistiky_kontrol;
PROCEDURE statistiky_pokut;

END pck_stat;
/


CREATE OR REPLACE PACKAGE BODY pck_stat
AS

PROCEDURE statistiky_provozoven
AS
BEGIN
LOCK TABLE provozovny IN SHARE MODE NOWAIT;
INSERT INTO stat_provozovny(k_datu, lokace, cleneni, pocet) VALUES (SYSDATE, 'česká republika', 'republika', (SELECT COUNT(*) FROM provozovny));
INSERT INTO stat_provozovny(k_datu, lokace, cleneni, pocet) SELECT SYSDATE, adr_kraj, 'kraj', COUNT(*) FROM provozovny GROUP BY adr_kraj;
INSERT INTO stat_provozovny(k_datu, lokace, cleneni, pocet) SELECT SYSDATE, adr_obec, 'obec', COUNT(*) FROM provozovny GROUP BY adr_obec;
INSERT INTO stat_provozovny(k_datu, lokace, cleneni, pocet) SELECT SYSDATE, adr_okres, 'okres', COUNT(*) FROM provozovny GROUP BY adr_okres;
COMMIT;
END statistiky_provozoven;
--EXEC pck_stat.statistiky_provozoven;
--SELECT * FROM STAT_PROVOZOVNY;

PROCEDURE statistiky_kontrol
AS
	p		NUMBER;	--pocet
	pz		NUMBER; 	--zavadne
	pn		NUMBER;		--nezavadne
	prz		NUMBER;	--procenta zavadne
	prn		NUMBER;	--procenta nezavadne
	PROCEDURE calc_perc
		AS
		BEGIN
		pn := p - pz;
		IF p = 0 THEN
			prz := 0;
			prn := 0;
		ELSE
			prz := ROUND(pz / p, 3);
			prn := ROUND(pn / p, 3);
		END IF;
		END;
BEGIN
LOCK TABLE kontroly, provozovny IN SHARE MODE NOWAIT;
--repubilka
SELECT COUNT(*) INTO p FROM kontroly;
SELECT COUNT(*) INTO pz FROM kontroly WHERE je_zavadna = 'Y';
calc_perc;
INSERT INTO stat_kontroly(k_datu, od, lokace, cleneni, pocet_zavadne, proc_zavadne, pocet_nezavadne, proc_nezavadne, pocet)
	VALUES (SYSDATE, NULL, 'česká republika', 'republika', pz, prz, pn, prn, p);
--kraje
FOR radek IN (SELECT adr_kraj FROM provozovny) LOOP
	SELECT COUNT(*) INTO p FROM kontroly JOIN provozovny USING(provozovny_id) WHERE adr_kraj = radek.adr_kraj;
	SELECT COUNT(*) INTO pz FROM kontroly JOIN provozovny USING(provozovny_id) WHERE adr_kraj = radek.adr_kraj AND je_zavadna = 'Y';
	calc_perc;
	INSERT INTO stat_kontroly(k_datu, od, lokace, cleneni, pocet_zavadne, proc_zavadne, pocet_nezavadne, proc_nezavadne, pocet)
	VALUES (SYSDATE, NULL, radek.adr_kraj, 'kraj', pz, prz, pn, prn, p);
END LOOP;

--okresy
FOR radek IN (SELECT adr_okres FROM provozovny) LOOP
	SELECT COUNT(*) INTO p FROM kontroly JOIN provozovny USING(provozovny_id) WHERE adr_okres = radek.adr_okres;
	SELECT COUNT(*) INTO pz FROM kontroly JOIN provozovny USING(provozovny_id) WHERE adr_okres = radek.adr_okres AND je_zavadna = 'Y';
	calc_perc;
	INSERT INTO stat_kontroly(k_datu, od, lokace, cleneni, pocet_zavadne, proc_zavadne, pocet_nezavadne, proc_nezavadne, pocet)
		VALUES (SYSDATE, NULL, radek.adr_okres, 'okres', pz, prz, pn, prn, p);		
END LOOP;

--obce
FOR radek IN (SELECT adr_obec FROM provozovny) LOOP
	SELECT COUNT(*) INTO p FROM kontroly JOIN provozovny USING(provozovny_id) WHERE adr_obec = radek.adr_obec;
	SELECT COUNT(*) INTO pz FROM kontroly JOIN provozovny USING(provozovny_id) WHERE adr_obec = radek.adr_obec AND je_zavadna = 'Y';
	calc_perc;
	INSERT INTO stat_kontroly(k_datu, od, lokace, cleneni, pocet_zavadne, proc_zavadne, pocet_nezavadne, proc_nezavadne, pocet)
		VALUES (SYSDATE, NULL, radek.adr_obec, 'obec', pz, prz, pn, prn, p);		
END LOOP;
COMMIT;
END statistiky_kontrol;
--EXEC PCK_STAT.STATISTIKY_KONTROL;
--SELECT * FROM STAT_KONTROLY;


PROCEDURE statistiky_pokut
AS
BEGIN
LOCK TABLE coi_sankce, kontroly, provozovny IN SHARE MODE NOWAIT;
INSERT INTO stat_pokuty(k_datu, od, lokace, cleneni, minimalni, maximalni, prumerna, suma, pocet_pokut)
	SELECT SYSDATE, NULL, 'česká republika', 'republika', MIN(vyse_pokuty), MAX(vyse_pokuty), AVG(vyse_pokuty), SUM(vyse_pokuty), COUNT(*) FROM coi_sankce;
INSERT INTO stat_pokuty(k_datu, od, lokace, cleneni, minimalni, maximalni, prumerna, suma, pocet_pokut)
	SELECT SYSDATE, NULL, adr_kraj, 'kraj', MIN(vyse_pokuty), MAX(vyse_pokuty), AVG(vyse_pokuty), SUM(vyse_pokuty), COUNT(*)
	FROM coi_sankce JOIN kontroly USING(kontroly_id) JOIN provozovny USING(provozovny_id) GROUP BY adr_kraj;
INSERT INTO stat_pokuty(k_datu, od, lokace, cleneni, minimalni, maximalni, prumerna, suma, pocet_pokut)
	SELECT SYSDATE, NULL, adr_okres, 'okres', MIN(vyse_pokuty), MAX(vyse_pokuty), AVG(vyse_pokuty), SUM(vyse_pokuty), COUNT(*)
	FROM coi_sankce JOIN kontroly USING(kontroly_id) JOIN provozovny USING(provozovny_id) GROUP BY adr_okres;
INSERT INTO stat_pokuty(k_datu, od, lokace, cleneni, minimalni, maximalni, prumerna, suma, pocet_pokut)
	SELECT SYSDATE, NULL, adr_obec, 'obec', MIN(vyse_pokuty), MAX(vyse_pokuty), AVG(vyse_pokuty), SUM(vyse_pokuty), COUNT(*)
	FROM coi_sankce JOIN kontroly USING(kontroly_id) JOIN provozovny USING(provozovny_id) GROUP BY adr_obec;
COMMIT;
END statistiky_pokut;
--EXEC pck_stat.statistiky_pokut;
--SELECT * FROM stat_pokuty;

END pck_stat;
/


