/*
	Author: Artur Finger
	
	Testovaci skript. Je potreba PL/SQL bloky a SQL prikazy poustet jednotlive (nikoliv cely skript najednou).
	
	-----------------------------------------
		--smazani vsech dat		
	DELETE FROM dle;
	DELETE FROM zakony;
	DELETE FROM coi_sankce;
	DELETE FROM coi_zakazy;
	DELETE FROM coi_zajisteni;
	DELETE FROM svs_zavady;
	DELETE FROM kontroly;
	DELETE FROM patri;
	DELETE FROM provozovny;
	DELETE FROM obory;
	
*/

PROMPT testovaci skrip =============================================<<<<<<<<<;
--vkladani malych dat 1/2
--provozovny a jejich obory zamereni
DECLARE
	id provozovny.provozovny_id%TYPE;
BEGIN

SELECT seq_provozovny.NEXTVAL INTO id FROM dual;
pck_pridej.provozovnu(id, 'Prodejna Hraček - Novák', 50.3, 15.01, '33213342', 'Vysočina', 'Vysočina', 'Nížina', 'Pod Jedlí', 12, '77399');
COMMIT;
pck_pridej.obor(id, 'Hračkářství');
COMMIT;

SELECT seq_provozovny.NEXTVAL INTO id FROM dual;
pck_pridej.provozovnu(id, 'Billa', 50.3, 15.01, '03145422', 'Severo Moravský', 'Šumperk', 'Šumperk', 'Poslká', 43, '22311');
COMMIT;
pck_pridej.obor(id, 'Velkoobchod');
pck_pridej.obor(id, '4610'); --CZ-NACE Velkoobchod smiseny
pck_pridej.obor(id, 'Skald potravin');
COMMIT;

SELECT seq_provozovny.NEXTVAL INTO id FROM dual;
pck_pridej.provozovnu(id, 'Billa', 50.3, 15.01, '55436623', 'Severo Moravský', 'Olomouc', 'Velká Bystřice', 'Dřevařská', 9, '77200');
COMMIT;
pck_pridej.obor(id, 'Velkoobchod');
pck_pridej.obor(id, '4610'); --CZ-NACE Velkoobchod smiseny
COMMIT;

SELECT seq_provozovny.NEXTVAL INTO id FROM dual;
pck_pridej.provozovnu(id, 'Sportovní potřeby SKIPPER', 50.3, 15.01, '93427836', 'Severo Moravský', 'Přerov', 'Brodek u Přerova', 'Husova', 17, '00122');
COMMIT;
pck_pridej.obor(id, 'Sportovní potřeby');
pck_pridej.obor(id, '4610'); --CZ-NACE Velkoobchod sportovnich potreb
COMMIT;

SELECT seq_provozovny.NEXTVAL INTO id FROM dual;
pck_pridej.provozovnu(id, 'Restaurace Master Chef', 51.3, 16.01, '60193328', 'Středo český', 'Praha', 'Praha', 'U Karolina', 3, '99887');
COMMIT;
pck_pridej.obor(id, '56100'); --CZ-NACE Stravování v restauracích, u stánků a v mobilních zařízeních
pck_pridej.obor(id, 'Sklad pokravin');
pck_pridej.obor(id, 'Příprava pokrmů');
pck_pridej.obor(id, 'Chlazení potravin');
COMMIT;


SELECT seq_provozovny.NEXTVAL INTO id FROM dual;
pck_pridej.provozovnu(id, 'Restaurace Grill', 51.3, 15.01, '12343328', 'Moravsko slezský', 'Ostrava', 'Ostrava', 'Dělnická', 13, '11887');
COMMIT;
pck_pridej.obor(id, '56100'); --CZ-NACE Stravování v restauracích, u stánků a v mobilních zařízeních
COMMIT;
END;
/
--data jsou vlozena vporadku
SELECT * FROM provozovny JOIN (patri JOIN obory USING (obory_id)) USING (provozovny_id);



--vkladani malych dat pokracuje 2/2
-- kontroly provadene na provozovnach
DECLARE
	idp provozovny.provozovny_id%TYPE;
	id kontroly.kontroly_id%TYPE;
BEGIN

idp := pck_ziskej.id_za_ico('33213342'); --Prodejna Hracek Novak
--3 nezavadne
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N', '01.02.2015');
pck_pridej.zakon(id, NULL, 'Nař. 1523/2007');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N');
pck_pridej.zakon(id, NULL, 'Zák. 56/2001');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp);
pck_pridej.zakon(id, NULL, 'Zák. 321/2001');
COMMIT;

SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'Y', '2.3.2015');
pck_pridej.zakon(id, NULL, 'Zák. 321/2001');
COMMIT;
pck_pridej.coi_san(id, 3000, '01.02.2016');
pck_pridej.coi_zaj(id, 'Pompo', 'Dřevěné korálky', 4, '§12;Zákon o bezpečnosti dětských hraček, prevence spolknutí.');
pck_pridej.coi_zak(id, 'Strašidelné želé', 20, 'kus', 630, NULL);
COMMIT;

idp := pck_ziskej.id_za_ico('55436623'); --Billa
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'Y');
COMMIT;
pck_pridej.zakon(id, NULL, 'Nař. 1523/2007');
pck_pridej.svs_zav(id, 'Nefunkční mrazící jednotky', 'Byly nalezené dva chladící boxy překračující nejvyšší povolenou teplotu o 10°C. Požadováno vřazení z provozu a udělena pokuta 2000 kč.');
pck_pridej.coi_zak(id, 'Vepřová směs', 142, 'kus', 12050, '§7;Zákon proti přidávání koňského masa do masných výrobků.');
COMMIT;

idp := pck_ziskej.id_za_ico('93427836'); --Sportovní potřeby
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'Y');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 1523/2003');
pck_pridej.coi_zaj(id, 'Adidas', 'Sportovní obuv', 12, '§1;Zákon o nelegálním prodeji padělků.');
COMMIT;

idp := pck_ziskej.id_za_ico('60193328'); --Restaurace masterchef
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N', '01.01.2014');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 1523/2003');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N', '05.05.2014');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 56/2001');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'Y', '23.08.2014');
COMMIT;
pck_pridej.zakon(id, NULL, 'Nař. 1523/2007');
pck_pridej.coi_san(id, 10000, '23.09.2014');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N', '17.11.2014');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 102/2001');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'Y', '06.11.2015');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 1523/2003');
pck_pridej.coi_san(id, 3000, '06.12.2015');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N', '23.12.2015');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 102/2001');
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'Y', '10.3.2016');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 1523/2003');
pck_pridej.coi_san(id, 7000, '10.04.2016');
COMMIT;

idp := pck_ziskej.id_za_ico('12343328'); --Restaurace Grill
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, idp, 'N');
COMMIT;
pck_pridej.zakon(id, NULL, 'Zák. 1523/2003');
COMMIT;
END;
/

--data jsou vlozena vporadku
SELECT * FROM coi_sankce JOIN kontroly USING (kontroly_id);
SELECT * FROM coi_zakazy JOIN kontroly USING (kontroly_id);
SELECT * FROM coi_zajisteni JOIN kontroly USING (kontroly_id);
SELECT * FROM svs_zavady JOIN kontroly USING (kontroly_id);







---------------------sbirani statistik (jednou za rok by se spustily tyto procedury), kt. naplni tabulky nize statistikami posbiranymi z dat
EXEC PCK_STAT.STATISTIKY_PROVOZOVEN;
EXEC PCK_STAT.STATISTIKY_KONTROL;
EXEC PCK_STAT.STATISTIKY_POKUT;

--nahled tabulek naplnenych statistikami
SELECT * FROM STAT_PROVOZOVNY;
SELECT * FROM STAT_KONTROLY;
SELECT * FROM STAT_POKUTY;


---------------------nahled na to, co klient databaze zobrazuje uzivateli
--(funguje na datech z tohoto skriptu, ale zajimavejsi je podivat se skrze tyto pohledy na 'data big.sql')

--specificke pohledy pro ico = 60193328 (Restaurace Master Chef)
SELECT * FROM V_DETAILY_KONTROL; 
SELECT * FROM V_INFO; 
SELECT * FROM V_OBORY;
SELECT * FROM V_POCET_ZAVAD; 
SELECT * FROM V_POCET_KONTROL;
SELECT * FROM V_SANKCE;
SELECT * FROM V_SANKCE_PRUMER;
SELECT * FROM V_TOP3SANKCE;  

--pohledy pocitane z cele databaze
SELECT * FROM V_KONTROLY;
SELECT * FROM V_KONTROLY_ZAVADNE; 
SELECT * FROM V_TOP10_SANKCE; 
SELECT * FROM V_TOP10_NEJKONTROLOVANEJSI;
SELECT * FROM V_TOP10_NEJZAVADNEJSICH;
SELECT * FROM V_TOP10_NEJSDRAZSICH_ZAKAZU;  
SELECT * FROM V_TOP10_NEJLEPSICH_RESTAURACI;
SELECT * FROM V_VELKOOBCHODY;  



PROMPT testovaci skript - errory =============================================<<<<<<<<<;

--priklady volani, ktera zpusobi error

DECLARE	id kontroly.kontroly_id%TYPE;
BEGIN
SELECT seq_kontroly.NEXTVAL INTO id FROM dual; --ico 33213342 = Prodejna Hracek Novak
pck_pridej.kontrolu(id, pck_ziskej.id_za_ico('33213342'), 'J'); --error: spatne pismenko, ma byt Y nebo N
END;
/
DECLARE	id kontroly.kontroly_id%TYPE;
BEGIN
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, pck_ziskej.id_za_ico('33213342'), 'N', '2.3.2015'); --error: datum ve spatnem formatu
END;
/
DECLARE	id kontroly.kontroly_id%TYPE;
BEGIN
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, pck_ziskej.id_za_ico('33213342'), 'N', '01.02.2018'); --error: datum moc vysoke
END;
/
DECLARE	id kontroly.kontroly_id%TYPE;
BEGIN
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, pck_ziskej.id_za_ico('33213342'), 'Y', '02.03.2015');
COMMIT;
pck_pridej.coi_san(id, 5000, '-21.213.2.'); --error: datum nelze naparsovat
END;
/
DECLARE	id kontroly.kontroly_id%TYPE;
BEGIN
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, pck_ziskej.id_za_ico('33213342'), 'Y', '02.03.2015');
COMMIT;
pck_pridej.coi_zaj(id, 'Mustang', 'autíčko', 0, '§12;Zákon o padělcích'); --error: mnozstvi musi byt vetsi nez 1 (table constraint)
END;
/
DECLARE	id kontroly.kontroly_id%TYPE;
BEGIN
SELECT seq_kontroly.NEXTVAL INTO id FROM dual;
pck_pridej.kontrolu(id, pck_ziskej.id_za_ico('33213342'), 'N', '05.03.2015');
COMMIT;
pck_pridej.coi_zaj(id, 'Nike', 'Mikina', 3, '§1;Zákon o nelegálním prodeji padělků.'); --error: trigger zakaze vlozeni zaznamu o coi_zajisteni k nezavadne kontrole
--chceme totiz, aby kontrola byla oznacena jako zavadna, kdyz k sobe ma nejake zajisteni/zavadu/zakaz/sankci
END;
/

